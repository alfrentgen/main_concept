#include "pch.h"
#include "rw.h"
#include "me.h"
#include "block.h"

#ifdef USE_INTRINSICS
#include "block_opt.h"
#endif

#define ME_BLOCK_SIZE 8
#define BL_LUMA_SIZE    (ME_BLOCK_SIZE*ME_BLOCK_SIZE)
#define BL_CHROMA_SIZE  ((ME_BLOCK_SIZE*ME_BLOCK_SIZE)/4)
#define BL_SIZE         (BL_LUMA_SIZE+2*BL_CHROMA_SIZE)
#define N_FRAMES 2

#ifndef SEARCH_STEP
#define SEARCH_STEP 2
#endif

int main(int argc, char** argv){
    //read file
    uint32_t width(1280);
    uint32_t height(720);
    if(argc < 2){
        LOG("Usage: main <file_name> [Width]x[Height]\n");
        return -1;
    }else if(argc < 3){
        LOG("Assumed resolution: %dx%d\n", width, height);
    }else{
        std::string resolution = std::string(argv[2]);
        std::regex pat("^(\\d{1,4})x(\\d{1,4})$");
        std::smatch result;
        if(std::regex_search(resolution, result, pat)){
            width = stoi(result[1]);
            height = stoi(result[2]);
        }else{
            LOG("Incorrect resolution has been given.\n");
            return -1;
        }
    }

    uint32_t nBlocksY = (height + ME_BLOCK_SIZE - 1)/ME_BLOCK_SIZE;
    uint32_t nBlocksX = (width + ME_BLOCK_SIZE - 1)/ME_BLOCK_SIZE;
    uint32_t dstHeight = nBlocksY * ME_BLOCK_SIZE;
    uint32_t dstWidth = nBlocksX * ME_BLOCK_SIZE;
    uint32_t lumaSize = dstHeight * dstWidth;
    uint32_t frameSize = lumaSize + lumaSize/2;
    std::vector<uint8_t> frames(N_FRAMES * frameSize, 0);

    int32_t res = readYV12Frames(argv[1], width, height, N_FRAMES,
                       frames.data(), dstWidth, dstHeight);

    if(res == -1){
        LOG("Cannot read input frames.\n");
    }
    width = dstWidth;
    height = dstHeight;

    uint8_t* refFrameYV12 = frames.data();
    uint8_t* curFrameYV12 = frames.data() + frameSize;
    std::vector<uint8_t> recFrameYV12(frameSize, 0);
    
    std::vector<uint8_t> refFrame(3*lumaSize, 0);
    std::vector<uint8_t> curFrame(3*lumaSize, 0);
    std::vector<uint8_t> recFrame(3*lumaSize, 0);
    
    yv12ToY444(refFrameYV12, width, height, refFrame.data());
    yv12ToY444(curFrameYV12, width, height, curFrame.data());
    
    std::vector<int16_t> difFrame(3*lumaSize, 0);
    int16_t* pDifFrame = difFrame.data();
    
    std::vector<uint8_t> refBlock(3*BL_LUMA_SIZE, 0);
    std::vector<uint8_t> curBlock(3*BL_LUMA_SIZE, 0);
    std::vector<int16_t> difBlock(3*BL_LUMA_SIZE, 0);
    uint8_t* pFrame(nullptr);
    uint8_t* pBlock(nullptr);
    
    MV mv;
    std::vector<MV> mvs(0);
    int32_t posX, posY;
    int32_t range = 16;
    
#ifdef RANDOM_MV    
    std::default_random_engine rndGen;
    typedef std::chrono::high_resolution_clock myclock;
    myclock::time_point beginning = myclock::now();
    myclock::duration d = myclock::now() - beginning;
    unsigned seed = d.count();
    rndGen.seed(seed);
#endif
    MV hpMV = {0,0};
    MV absMV;
    std::vector<uint8_t> hpBlock(3*BL_LUMA_SIZE, 0);
    std::vector<MV> hpMVs(0);
    for(int32_t y = 0; y < nBlocksY; y++){
        for(int32_t x = 0; x < nBlocksX; x++){
            posY = y * ME_BLOCK_SIZE;
            posX = x * ME_BLOCK_SIZE;
            
            pFrame = curFrame.data() + posY * width + posX;
            readBlockY444(pFrame, curBlock.data(), width, height, ME_BLOCK_SIZE, ME_BLOCK_SIZE);
#ifdef RANDOM_MV
            mv = getRandomMV(width, height, ME_BLOCK_SIZE, ME_BLOCK_SIZE, posY, posX, range, rndGen);
#else
#ifdef OPT_SEARCH
            /*LOG("mv{%d, %d}, pos{%d, %d}\n",mv.x, mv.y, posX, posY);
            LOG("Fastsearch for pos{%d, %d}\n", posX, posY);*/
            mv = fastSearchMV(refFrame.data(), width, height, curBlock.data(), 
                              ME_BLOCK_SIZE, ME_BLOCK_SIZE, posY, posX, range, SEARCH_STEP);
#else
            mv = fullSearchMV(refFrame.data(), width, height, curBlock.data(), 
                            ME_BLOCK_SIZE, ME_BLOCK_SIZE, posY, posX, range);
#endif
#endif
            if(checkMV(posY, posX, mv, ME_BLOCK_SIZE, ME_BLOCK_SIZE, width, height)){
                return -1;
            }
            mvs.push_back(mv);
            
            pFrame = refFrame.data() + (posY + mv.y) * width + posX + mv.x;
            readBlockY444(pFrame, refBlock.data(), width, height, ME_BLOCK_SIZE, ME_BLOCK_SIZE);
            
#ifdef HALF_PEL
            absMV = mv;
            absMV.x += posX;
            absMV.y += posY;
            hpMV = hpSearchY444(refFrame.data(), width, height, curBlock.data(), ME_BLOCK_SIZE, ME_BLOCK_SIZE, absMV, hpBlock.data());
            hpMVs.push_back(hpMV);
            /*LOG("Pos{%d, %d}: int-pel mv{x=%d,y=%d,SAD=%d}, ", posX, posY, mv.x, mv.y, mv.sad);
            LOG("half-pel mv{x=%d,y=%d,SAD=%d}\n", hpMV.x, hpMV.y, hpMV.sad);*/
            
            if(hpMV.x | hpMV.y){
                MV posB1 = {absMV.x, absMV.y};
                MV posB2 = {absMV.x + hpMV.x, absMV.y + hpMV.y};
                interpolateBlockY444(refFrame.data(), width, height, ME_BLOCK_SIZE, ME_BLOCK_SIZE, posB1, posB2, refBlock.data());
                /*if(mv.sad > hpMV.sad){
                    LOG("Pos{%d, %d}: int-pel mv{x=%d,y=%d,SAD=%d}, ", posX, posY, mv.x, mv.y, mv.sad);
                    LOG("half-pel mv{x=%d,y=%d,SAD=%d}\n", hpMV.x, hpMV.y, hpMV.sad);
                }*/
            }
#endif
            
            pFrame = recFrame.data() + posY * width + posX;
            writeBlockY444(refBlock.data(), pFrame, width, height, ME_BLOCK_SIZE, ME_BLOCK_SIZE);
            
            calcDiff<uint8_t, int16_t>(refBlock.data(), curBlock.data(), difBlock.data(), 3*BL_LUMA_SIZE);
            pDifFrame = difFrame.data() + posY * width + posX;
            writeBlockY444(difBlock.data(), pDifFrame, width, height, ME_BLOCK_SIZE, ME_BLOCK_SIZE);
        }
    }

    double yPSNR(0);
    double uPSNR(0);
    double vPSNR(0);
    y444ToYV12(recFrame.data(), width, height, recFrameYV12.data());
    calcPSNRYV12(recFrameYV12.data(), curFrameYV12, width, height, yPSNR, uPSNR, vPSNR);
    LOG("PSNR_Y = %f, PSNR_U = %f, PSNR_V = %f\n", yPSNR, uPSNR, vPSNR);

#define DUMP
#ifdef DUMP
    res = dumpOutput(frames.data(), frames.size(), "inFrames.yuv");
    
    refFrame.insert(refFrame.end(), curFrame.begin(), curFrame.end());
    res |= dumpOutput(refFrame.data(), refFrame.size(), "y444Frames.yuv");
    
    res |= dumpOutput((uint8_t*)difFrame.data(), difFrame.size()*sizeof(int16_t), "diffY444.yuv");
    std::vector<int16_t> difFrameYV12(frameSize, 0);
    y444ToYV12(difFrame.data(), width, height, difFrameYV12.data());
    res |= dumpOutput((uint8_t*)difFrameYV12.data(), difFrameYV12.size()*sizeof(int16_t), "diffYV12.yuv");
    
    res |= dumpOutput(recFrame.data(), recFrame.size(), "recY444.yuv");
    res |= dumpOutput(recFrameYV12.data(), recFrameYV12.size(), "recYV12.yuv");

    if(res == -1){
        LOG("Cannot write dump files.\n");
    }
#endif
}
