#include "pch.h"
#include "me.h"
#include "block.h"

#ifdef USE_INTRINSICS
#include<emmintrin.h>
#endif

#ifdef USE_INTRINSICS

void readBlock8x8(uint8_t* pSrc, uint8_t* pBl, uint32_t srcWidth, uint32_t blWidth, uint32_t blHeight){
    __m128i xmms[8];
    for(int i = 0; i < 8; i++){
        xmms[i] = _mm_loadl_epi64((__m128i const*)pSrc);
        pSrc += srcWidth;
    }
    for(int i = 0; i < 8; i++){
        _mm_storel_epi64 ((__m128i*)pBl, xmms[i]);
        pBl += blWidth;
    }
}

template<>
void readBlock(uint8_t* pSrc, uint8_t* pBlock, uint32_t srcWidth, uint32_t blWidth, uint32_t blHeight){
    if(blWidth == 8 && blHeight == 8){
        readBlock8x8(pSrc, pBlock, srcWidth, blWidth, blHeight);
    }else{
        for(uint32_t y = 0 ; y < blHeight; y++){
            std::copy_n(pSrc, blWidth, pBlock);
            pSrc += srcWidth;
            pBlock += blWidth;
        }
    }
}
#endif
