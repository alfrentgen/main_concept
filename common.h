#pragma once

struct Coordinate {
    int32_t x;
    int32_t y;
};

struct MV{
    Coordinate crd;
    Coordinate hpCrd;
    uint64_t sad;
};

template<typename T>
class Block{
public:
    Coordinate m_coord;
    uint32_t m_width[3];
    uint32_t m_height[3];
    std::vector<T> m_pels;
    T* m_pFrame;
    T* m_pPlanes[3];
    MV m_mv;
};

template<typename T>
class BlockYV12 : public Block
{
public:
    BlockYV12(uint32_t width, uint32_t height, int32_t posX, int32_t posY, bool allocPels = false);
    SetSize(uint32_t width, uint32_t height);
    void AllocatePels();
};

BlockYV12::BlockYV12(uint32_t width, uint32_t height, int32_t posX, int32_t posY, bool allocPels) :
    m_pFrame(nullptr), m_pPlanes({nullptr}), m_pels(0), m_coord(posX,posY), m_mv({0,0}, {0,0}, 0)
{
    SetSize(width, height);
    if(allocPels){
        AllocatePels();
    }
}

BlockYV12::SetSize(uint32_t width, uint32_t height){
    m_width[0] = width;
    m_height[0]= height;
    for(int p = 1; p < 3; p++){
        m_width[p] = width/2;
        m_height[p]= height/2;
    }
}

void BlockYV12::AllocatePels(){
    size_t size(m_width[0] * m_height[0]);
    for(int p = 1; p < 3; p++){
        size += m_width[p]*m_height[p];
    }
    m_pels.resize(size, 0);
}
