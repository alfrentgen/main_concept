#include "pch.h"
#include "rw.h"

int32_t readYV12Frames(char* fileName, uint32_t fWidth, uint32_t fHeight, uint32_t nFrames,
                       uint8_t* pDst, uint32_t dstWidth, uint32_t dstHeight){
    if(fWidth%2||fHeight%2||dstWidth%2||dstHeight%2){
        return -1;
    }
    std::ifstream ifs;
    ifs.open(fileName, std::ios::in | std::ios::binary);
    if(!ifs.good()){
        return -1;
    }
    
    std::vector<uint8_t> buffer(fWidth, 0);
    uint32_t bFill = dstHeight - fHeight;
    uint32_t rFill = dstWidth - fWidth;
    uint32_t dstWidthCh = dstWidth/2;
    uint32_t fHeightCh = fHeight/2;
    uint32_t fWidthCh = fWidth/2;
    uint32_t rFillCh = rFill/2;
    uint32_t bFillCh = bFill/2;
    for(int32_t n = 0; n < nFrames; n++){
        //Y
        for(int32_t y = 0; y < fHeight; y++){
            ifs.read((char*)buffer.data(), fWidth);
            if(!ifs.good()){
                ifs.close();
                return -1;
            }
            std::copy_n(buffer.data(), fWidth, pDst);
            if(rFill){
                std::fill_n(pDst + fWidth, rFill, pDst[fWidth-1]);
            }
            pDst += dstWidth;
        }
        for(int32_t y = 0; y < bFill; y++){
            std::copy_n(buffer.data(), dstWidth, pDst);
            pDst += dstWidth;
        }
        //U, V
        for(int nChPlanes = 0; nChPlanes < 2; nChPlanes++){
            for(int32_t y = 0; y < fHeightCh; y++){
                ifs.read((char*)buffer.data(), fWidthCh);
                if(!ifs.good()){
                    ifs.close();
                    return -1;
                }
                std::copy_n(buffer.data(), fWidthCh, pDst);
                if(rFillCh){
                    std::fill_n(pDst + fWidthCh, rFillCh, pDst[fWidthCh-1]);
                }
                pDst += dstWidthCh;
            }
            for(int32_t y = 0; y < bFillCh; y++){
                std::copy_n(buffer.data(), dstWidthCh, pDst);
                pDst += dstWidthCh;
            }
        }
    }

    ifs.close();
    return 0;
}

int32_t readYV12Frame(std::ifstream& ifs, uint32_t fWidth, uint32_t fHeight, uint32_t nFrames,
                       uint8_t* pDst, uint32_t dstWidth, uint32_t dstHeight){
    if(fWidth%2||fHeight%2||dstWidth%2||dstHeight%2){
        return -1;
    }

    if(!ifs.good() || !ifs.is_open()){
        return -1;
    }
    
    std::vector<uint8_t> buffer(fWidth, 0);
    uint32_t bFill = dstHeight - fHeight;
    uint32_t rFill = dstWidth - fWidth;
    uint32_t dstWidthCh = dstWidth/2;
    uint32_t fHeightCh = fHeight/2;
    uint32_t fWidthCh = fWidth/2;
    uint32_t rFillCh = rFill/2;
    uint32_t bFillCh = bFill/2;
    for(int32_t n = 0; n < nFrames; n++){
        //Y
        for(int32_t y = 0; y < fHeight; y++){
            ifs.read((char*)buffer.data(), fWidth);
            if(!ifs.good()){
                ifs.close();
                return -1;
            }
            std::copy_n(buffer.data(), fWidth, pDst);
            if(rFill){
                std::fill_n(pDst + fWidth, rFill, pDst[fWidth-1]);
            }
            pDst += dstWidth;
        }
        for(int32_t y = 0; y < bFill; y++){
            std::copy_n(buffer.data(), dstWidth, pDst);
            pDst += dstWidth;
        }
        //U, V
        for(int nChPlanes = 0; nChPlanes < 2; nChPlanes++){
            for(int32_t y = 0; y < fHeightCh; y++){
                ifs.read((char*)buffer.data(), fWidthCh);
                if(!ifs.good()){
                    ifs.close();
                    return -1;
                }
                std::copy_n(buffer.data(), fWidthCh, pDst);
                if(rFillCh){
                    std::fill_n(pDst + fWidthCh, rFillCh, pDst[fWidthCh-1]);
                }
                pDst += dstWidthCh;
            }
            for(int32_t y = 0; y < bFillCh; y++){
                std::copy_n(buffer.data(), dstWidthCh, pDst);
                pDst += dstWidthCh;
            }
        }
    }

    ifs.close();
    return 0;
}


int32_t dumpOutput(uint8_t* pData, size_t length, char* fileName){
    std::ofstream ofs;
    ofs.open(fileName, std::ios::out | std::ios::binary);
    if(!ofs.good()){
        ofs.close();
        return -1;
    }
    ofs.write((char*)pData, length);
    ofs.flush();
    ofs.close();
    return 0;
}
