#pragma once
#include<cmath>

template<typename T1, typename T2>
void calcDiff(T1* ref, T1* cur, T2* dif, size_t length){
    for(size_t i = 0; i < length; i++){
        dif[i] = (T2)cur[i] - (T2)ref[i];
    }
}

template<typename T>
void readBlock(T* pSrc, T* pBlock, uint32_t srcStride, uint32_t blWidth, uint32_t blHeight){
    for(uint32_t y = 0 ; y < blHeight; y++){
#ifdef UNOPTIMIZED_BLOCK_RW
        for(uint32_t x = 0 ; x < blWidth; x++){
            *pBlock = pSrc[y*srcStride + x];
            pBlock++;
        }
#else
        std::copy_n(pSrc, blWidth, pBlock);
        pSrc += srcStride;
        pBlock += blWidth;
#endif
    }
}

template<typename T>
void readBlockY444(T* pSrc, T* pBlock, uint32_t srcWidth, uint32_t srcHeight, uint32_t blWidth, uint32_t blHeight){
    size_t srcSize = srcWidth * srcHeight;
    size_t blSize = blWidth * blHeight;
    for(int32_t p = 0; p < 3; p++){
        readBlock(pSrc, pBlock, srcWidth, blWidth, blHeight);
        pSrc+=srcSize;
        pBlock+=blSize;
    }
}

template<typename T>
void writeBlock(T* pBlock, T* pDst, uint32_t dstStride, uint32_t blWidth, uint32_t blHeight){
    for(uint32_t y = 0 ; y < blHeight; y++){
#ifdef UNOPTIMIZED_BLOCK_RW
        for(uint32_t x = 0 ; x < blWidth; x++){
            pDst[y*dstStride + x] = pBlock[y*blWidth + x];
        }
#else
        std::copy_n(pBlock, blWidth, pDst);
        pDst += dstStride;
        pBlock += blWidth;
#endif
    }
}

template<typename T>
void writeBlockY444(T* pBlock, T* pDst, uint32_t dstWidth, uint32_t dstHeight, uint32_t blWidth, uint32_t blHeight){
    size_t dstSize = dstWidth * dstHeight;
    size_t blSize = blWidth * blHeight;
    for(int32_t p = 0; p < 3; p++){
        writeBlock(pBlock, pDst, dstWidth, blWidth, blHeight);
        pDst+=dstSize;
        pBlock+=blSize;
    }
}
