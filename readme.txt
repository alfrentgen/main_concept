Configure and build(GCC 5 or higher, CMake 3.0 or higher):

CMake project options:
-DRANDOM_MV - enables random motion vector generation(disables -DFAST_SEARCH)
-DFAST_SEARCH - enables optimized search, if disabled full search is used
-DHPEL_SEARCH - enables half-pel search
-DUSE_INTRINSICS - enables Intel Intrinsics
-DDEBUG_INFO - enables "-g -O1" for GCC compiler.

mkdir ./build
cd ./build
cmake -DFAST_SEARCH=ON -DHPEL_SEARCH=ON -DUSE_INTRINSICS=ON -G "Unix Makefiles" ..
make

or via CMake GUI

Run:
./main ../rocket_1280x720_P420.yuv 1280x720

Текст исходного задания в файле homework.txt

Основная часть выполнена. Примечание: внутри исходные данные конвертируются в из YV12(Y420) в Y444 и обратно.
Выходные файлы: recYV12.yuv recY444.yuv - 8-ми битные данные содержат восстановленный второй кадр, diffYV12.yuv и diffY444.yuv - 16-ти битные содержат разность между первым и вторым кадром, inFrames.yuv и y444Frames.yuv - 8-ми битные входные кадры. Форматы YV12(Y420) и Y444 соответствено.

Дополнительные задания:
1) выполнено с использованием "Intel Intrinsics"
2) не выполнено
3) Исходные кадры в файле "rocket_1280x720_P420.yuv"
4) функция fullSearchMV() в me.cpp
5) не выполнено
6) Алгоритм с оптимизацией по скорости поиска(возможно изменять шаг) поиска, функция fastSearchMV() в me.cpp
7) полпиксельный поиск функция MV hpSearchY444() в me.cpp
8) функция calcPSNRYV12() в rw.h, расчитывает PSNR для трех плоскостей.
9) не выполнено
