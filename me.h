#pragma once

struct MV{
    int32_t x;
    int32_t y;
    uint64_t sad;
};

MV getRandomMV(int32_t width, int32_t height, 
                int32_t blWidth, int32_t blHeight, 
                int32_t posY, int32_t posX, int32_t range, std::default_random_engine& gen);

int32_t checkMV(int32_t posY, int32_t posX, MV mv, 
                int32_t blWidth, int32_t blHeight, int32_t width, int32_t height);

MV fullSearchMV(uint8_t* pFrame, int32_t fWidth, int32_t fHeight, 
                uint8_t* pBlock, int32_t blWidth, int32_t blHeight, 
                int32_t posY, int32_t posX, int32_t radius, int32_t step = 1);

MV fastSearchMV(uint8_t* pFrame, int32_t fWidth, int32_t fHeight, 
                uint8_t* pBlock, int32_t blWidth, int32_t blHeight, 
                int32_t posY, int32_t posX, int32_t range, int32_t step);

MV hpSearchY444(uint8_t* pRefFrame, int32_t fWidth, int32_t fHeight, 
                uint8_t* pCurBlock, int32_t blWidth, int32_t blHeight, 
                MV absMV, uint8_t* pBuffer);

void interpolateBlockY444(uint8_t* pFrame, int32_t fWidth, int32_t fHeight, 
                          int32_t blWidth, int32_t blHeight, MV posB1, MV posB2, uint8_t* pBlock);