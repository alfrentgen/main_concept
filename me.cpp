#include "pch.h"
#include "me.h"
#include "block.h"

#ifdef USE_INTRINSICS
#include "block_opt.h"
#endif

struct mv_range{
  int32_t left;
  int32_t right;
  int32_t top;
  int32_t bottom;
};

mv_range getMVRange(int32_t fWidth, int32_t fHeight, 
                    int32_t blWidth, int32_t blHeight, 
                    int32_t blPosY, int32_t blPosX, int32_t range){
    int32_t x1 = blPosX; 
    int32_t y1 = blPosY;
    int32_t x2 = blPosX + blWidth;
    int32_t y2 = blPosY + blHeight;
    mv_range r = {0, 0, 0, 0};
    r.left = (x1-range) < 0 ? 0 : x1-range;
    r.right = (x2+range+1) > fWidth ? fWidth : x2+range+1;
    r.right -= blWidth;
    r.top = (y1-range) < 0 ? 0 : y1-range;
    r.bottom = (y2+range+1) > fHeight ?fHeight : y2+range+1;
    r.bottom -= blHeight;    
    return r;
}

MV getRandomMV(int32_t fWidth, int32_t fHeight, 
                int32_t blWidth, int32_t blHeight, 
                int32_t posY, int32_t posX, int32_t range, std::default_random_engine& generator)
{
    mv_range r = getMVRange(fWidth, fHeight, blWidth, blHeight, posY, posX, range);
    
    std::uniform_int_distribution<int> distributionX(r.left, r.right);
    std::uniform_int_distribution<int> distributionY(r.top, r.bottom);
    MV mv = {0,0};
    mv.x = distributionX(generator) - posX;
    mv.y = distributionY(generator) - posY;
    if(r.left < 0 || r.top < 0 || r.right > fWidth || r.bottom >fHeight){
        LOG("left=%d right=%d\n", r.left, r.right);
        LOG("top=%d bottom=%d\n", r.top, r.bottom);
        LOG("x=%d y=%d\n", mv.x, mv.y);
        exit(0);
    }
    return mv;
}

uint64_t calcSad(uint8_t* pRef, int32_t fWidth, uint8_t* pCur, int32_t blWidth, int32_t blHeight){
    uint64_t sad(0);
    for(int32_t blY=0; blY < blHeight; blY++){//block y
        for(int32_t blX=0; blX < blWidth; blX++){//block x
            sad += abs(pCur[blY*blWidth + blX] - pRef[blY*fWidth + blX]);
        }
    }
    return sad;
}

void interpolateBlock(uint8_t* pSrc, int32_t stride, int32_t blWidth, int32_t blHeight, MV posB1, MV posB2, uint8_t* pBlock){
    uint8_t* pBl1 = pSrc + posB1.y * stride + posB1.x;
    uint8_t* pBl2 = pSrc + posB2.y * stride + posB2.x;
    for(uint32_t y = 0 ; y < blHeight; y++){
        for(uint32_t x = 0 ; x < blWidth; x++){
            *pBlock = ((uint16_t)pBl1[y * stride + x] + pBl2[y * stride + x])/2;
            pBlock++;
        }
    }
}

void interpolateBlockY444(uint8_t* pFrame, int32_t fWidth, int32_t fHeight, int32_t blWidth, int32_t blHeight, MV posB1, MV posB2, uint8_t* pBlock){
    size_t frameSize = fWidth * fHeight;
    size_t blockSize = blWidth * blHeight;
    for(int p = 0; p < 3; p++){
        interpolateBlock(pFrame, fWidth, blWidth, blHeight, posB1, posB2, pBlock);
        pFrame += frameSize;
        pBlock += blockSize;
    }
}

MV hpSearchY444(uint8_t* pRefFrame, int32_t fWidth, int32_t fHeight, 
                uint8_t* pCurBlock, int32_t blWidth, int32_t blHeight, 
                MV absMV, uint8_t* pBuffer)
{
    int32_t posX = absMV.x;
    int32_t posY = absMV.y;
    size_t blSize = blWidth * blHeight;
    mv_range r = getMVRange(fWidth, fHeight, blWidth, blHeight, posY, posX, 1);
    //LOG("absMV{%d, %d}, pos{%d, %d}\n", absMV.x, absMV.y, posX, posY);
    //LOG("H-pel range:%d, %d, %d, %d\n", r.left, r.top, r.right, r.bottom);
    
    MV posB1 = { .x=posX, .y=posY};
    MV posB2;
    uint8_t* pRefBlock = pBuffer;
    MV hpMV = { 0, 0, absMV.sad};
    uint64_t prevSAD(absMV.sad);
    uint64_t curSAD;
    for(int32_t y=r.top; y < r.bottom; y++){
        for(int32_t x=r.left; x < r.right; x++){
            curSAD = 0;
            if(y==posY && x==posX){
                continue;//skip base position
            }
            posB2 = {x, y};
            interpolateBlockY444(pRefFrame, fWidth, fHeight, blWidth, blHeight, posB1, posB2, pRefBlock);
            for(int32_t p = 0; p < 3; p++){//YUV planes
                curSAD += calcSad(pRefBlock + p*blSize, blWidth, pCurBlock + p*blSize, blWidth, blHeight);
            }
            //if((posY - y < 0) || (posX - x < 0)){
            //LOG("H-pel:%d, %d\n", x - posX, y - posY);
            //LOG("H-pel pos:%d, %d\n", x, y);
            //}
            
            if(curSAD < prevSAD){
                prevSAD = curSAD;
                hpMV.sad = curSAD;
                hpMV.y = y - posY;
                hpMV.x = x - posX;
            }
        }
            
    }
    return hpMV;
}

MV fullSearchMV(uint8_t* pFrame, int32_t fWidth, int32_t fHeight, uint8_t* pBlock, int32_t blWidth, int32_t blHeight, int32_t posY, int32_t posX, int32_t radius, int32_t step)
{
    int32_t blSize = blWidth*blHeight;
    int32_t fSize = fWidth*fHeight;    
    mv_range r = getMVRange(fWidth, fHeight, blWidth, blHeight, posY, posX, radius);
    
    uint8_t* pRef(nullptr);
    uint8_t* pCur(nullptr);
    uint64_t sadPrev(std::numeric_limits<uint64_t>::max());
    uint64_t sadCur(0);
    MV mv = {0,0};
    for(int32_t y=r.top; y < r.bottom; y+=step){//frame y
        for(int32_t x=r.left; x < r.right; x+=step){//frame x
            pRef = pFrame + y * fWidth + x;
            pCur = pBlock;
            sadCur = 0;
            for(int32_t p = 0; p < 3; p++){//YUV planes
                sadCur += calcSad(pRef, fWidth, pCur, blWidth, blHeight);
                pRef += fSize;
                pCur += blSize;
            }
            if(sadCur < sadPrev){
                sadPrev = sadCur;
                mv.sad = sadCur;
                mv.x = x;
                mv.y = y;
            }
        }
    }

    mv.x = mv.x - posX;
    mv.y = mv.y - posY;
    return mv;
}

MV fastSearchMV(uint8_t* pFrame, int32_t fWidth, int32_t fHeight, 
                uint8_t* pBlock, int32_t blWidth, int32_t blHeight, 
                int32_t posY, int32_t posX, int32_t range, int32_t step)
{
    MV mv1 = fullSearchMV(pFrame, fWidth, fHeight, pBlock, blWidth, blHeight, posY, posX, range, step);
    posY = posY + mv1.y;
    posX = posX + mv1.x;
    //LOG("mv{%d, %d}, pos{%d, %d}\n",mv1.x, mv1.y, posX, posY);
    MV mv2 = fullSearchMV(pFrame, fWidth, fHeight, pBlock, blWidth, blHeight, posY, posX, step);
    mv1.x += mv2.x;
    mv1.y += mv2.y;
    //LOG("mv{%d, %d}, pos{%d, %d}\n",mv1.x, mv1.y, posX, posY);
    return mv1;
}

int32_t checkMV(int32_t posY, int32_t posX, MV mv, 
                int32_t blWidth, int32_t blHeight, int32_t width, int32_t height)
{
    int32_t res = 0;
    if(posY + mv.y < 0){
        LOG("Top border!!!\n");
        res = 0x1;
    }
    if(posY + mv.y + blHeight > height){
        LOG("Bottom border!!!\n");
        res = 0x2;
    }

    if(posX + mv.x < 0){
        LOG("Left border!!!\n");
        res = 0x4;
    }
    
    if(posX + mv.x + blWidth > width){
        LOG("Right border!!!\n");
        res = 0x8;
    }
    
    if(res){
        LOG("PosY=%d, PosX=%d; ySize=%d, xSize=%d; mv.y=%d, mv.x=%d\n", posY, posX, blHeight, blWidth, mv.y, mv.x);
    }
    
    return res;
}
