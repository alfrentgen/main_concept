#pragma once

int32_t readYV12Frames(char* fileName, uint32_t fWidth, uint32_t fHeight, uint32_t nFrames,
                       uint8_t* pDst, uint32_t dstWidth, uint32_t dstHeight);

int32_t readYV12Frame(std::ifstream& ifs, uint32_t fWidth, uint32_t fHeight, uint32_t nFrames,
                       uint8_t* pDst, uint32_t dstWidth, uint32_t dstHeight);

int32_t dumpOutput(uint8_t* pData, size_t length, char* fileName);

template<typename T>
void yv12ToY444(T* pYV12Frame, uint32_t width, uint32_t height, T* pY444Frame){
    size_t lumaSize = width*height;
    std::copy_n(pYV12Frame, lumaSize, pY444Frame);
    pYV12Frame += lumaSize;
    pY444Frame += lumaSize;
    for(int ch = 0; ch < 2; ch++){
        for(uint32_t h = 0; h < height/2; h++){
            for(int w = 0; w < width/2; w++){
                pY444Frame[2*w] = pYV12Frame[w];
                pY444Frame[2*w+1] = pYV12Frame[w];
            }
            std::copy_n(pY444Frame, width, pY444Frame+width);
            pYV12Frame += width/2;
            pY444Frame += 2*width;
        }
    }
}

template<typename T>
void y444ToYV12(T* pY444Frame, uint32_t width, uint32_t height, T* pYV12Frame){
    size_t lumaSize = width*height;
    std::copy_n(pY444Frame, lumaSize, pYV12Frame);
    pYV12Frame += lumaSize;
    pY444Frame += lumaSize;
    for(int32_t ch = 0; ch < 2; ch++){
        for(uint32_t h = 0; h < height/2; h++){
            for(int w = 0; w < width/2; w++){
                pYV12Frame[w] = ((int64_t)pY444Frame[2*w] + pY444Frame[2*w+1] + 
                pY444Frame[width + 2*w] + pY444Frame[width + 2*w+1])/4;
            }
            pYV12Frame += width/2;
            pY444Frame += 2*width;
        }
    }
}

template<typename T>
double calcPSNR(T* ref, T* cur, size_t length, T maximum = 0){
    double psnr(0);
    double max = (maximum) ? maximum : std::numeric_limits<T>::max();
    double mse(0);
    double diff(0);
    for(size_t i = 0; i < length; i++){
        diff = ref[i] - cur[i];
        mse += diff*diff;
    }
    mse /= length;
    psnr = 10*log10((max*max)/mse);
    return psnr;
}

template<typename T>
void calcPSNRYV12(T* ref, uint8_t* cur, int32_t width, int32_t height, 
                  double& psnrY, double& psnrU, double& psnrV){
    size_t offset = 0;
    int32_t frameSize = width * height;
    psnrY = calcPSNR(ref, cur, frameSize);
    offset = frameSize;
    psnrU = calcPSNR(ref + offset, cur + offset, frameSize/4);
    offset += frameSize/4;
    psnrV = calcPSNR(ref + offset, cur + offset, frameSize/4);
}
