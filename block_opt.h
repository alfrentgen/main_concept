#pragma once

#ifdef USE_INTRINSICS
extern template
void readBlock<uint8_t>(uint8_t* pSrc, uint8_t* pBlock, uint32_t srcWidth, uint32_t blWidth, uint32_t blHeight);
#endif
